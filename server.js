require('dotenv').config()

const User            = require('./models/users');
const Post            = require('./models/posts');
const Catalog         = require('./models/catalog');
const Comments        = require('./models/comments');
const Like            = require('./models/likes');
const Files           = require('./models/files');
const Link            = require('./models/link');
const Search          = require('./models/search');
const express         = require('express');
const path            = require('path'); // модуль для парсинга пути
const favicon         = require('serve-favicon');
const bodyParser      = require('body-parser');
const urlencodedParser= bodyParser.urlencoded({extended: false});
const methodOverride  = require('method-override');
const useragent       = require('express-useragent');
const cors            = require('cors');
const fileUpload      = require('express-fileupload');
const app             = express();

app.use(bodyParser.json());
app.use(methodOverride());
app.use(useragent.express());

app.use('/tmp/', express.static('tmp'));
app.use(fileUpload({
    tempFileDir: '/tmp/'
}));

app.use(cors());

app.use(function (req, res, next) {
    let allowedOrigins = ['http://www.deti-media.ru', 'http://deti-media.ru', 'http://89.111.132.249', 'http://localhost:3000'];
    let origin = req.headers.origin;
    if(allowedOrigins.indexOf(origin) > -1){
        res.header('Access-Control-Allow-Origin', origin);
    }
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, HEAD, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Authorization, Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Товар по фильтрам
app.post("/catalogfilter", urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Catalog.getGoodByFilter(req, res, req.body)
});

//Изменить товар
app.put("/catalog", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Catalog.updateGood(req, res, req.body)
});

//Удалить товар
app.delete("/catalog", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Catalog.deleteGood(req, res, req.body)
});

//Создать товар
app.post("/catalog", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Catalog.addGood(req, res, req.body)
});

//Получить товар по id автора
app.post("/catalogbyauthorid", urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Catalog.getGoodsByAuthorId(req, res, req.body)
});

//Получить товар по id
app.get("/catalogbyid", urlencodedParser, (req, res) => {
    Catalog.getGoodById(req, res)
});

//Получить товары
app.get("/catalog", urlencodedParser, (req, res) => {
    Catalog.getGoods(req, res)
});


app.post("/search", urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Search.searchByAuthorAndPostTitle(req, res, req.body)
});

app.get('/link', urlencodedParser, (req, res) => {
    if(!req.query.url) return res.sendStatus(400);
    Link.FetchLink(req, res)
})

app.options('/file_upload', cors())
//Загрузить файл на сервер
app.post('/file_upload', urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Files.uploadByFile(req, res)
})

//Получить лайки с записи
app.post('/likes_count', urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Like.getLikesFromPost(req, res, req.body)
})

//Поставить/снять лайк с записи
app.post('/like', urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Like.giveLike(req, res, req.body)
})

//Авторизация пользователя
app.post('/sign_in', urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    User.authUser(req, res, req.body)
})

//Регистрация пользователя
app.post('/sign_up', urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    User.regUser(req, res, req.body)
})

//Изменить запись
app.put("/posts", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Post.updatePost(req, res, req.body)
});

//Удалить запись
app.delete("/posts", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Post.deletePost(req, res, req.body)
});

//Создать запись
app.post("/posts", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Post.addPost(req, res, req.body)
});

//Получить запись по id
app.get("/postbyid", urlencodedParser, (req, res) => {
    Post.getPostById(req, res)
});

//Получить запись по id автора
app.post("/postbyauthorid", urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Post.getPostsByAuthorId(req, res, req.body)
});

//Получить все записи
app.get("/all_posts", urlencodedParser, (req, res) => {
    Post.getAllPosts(req, res)
});


//Получить записи
app.get("/posts", urlencodedParser, (req, res) => {
    Post.getPosts(req, res)
});

//Получить главные записи
app.get("/main_posts", urlencodedParser, (req, res) => {
    Post.getMainPosts(req, res)
});

//Изменить комментарий
app.put("/comments", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Comments.updateComment(req, res, req.body)
});

//Удалить комментарий
app.delete("/comments", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Comments.deleteComment(req, res, req.body)
});

//Создать комментарий
app.post("/comments", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Comments.addComment(req, res, req.body)
});

//Получить комментарии к записи
app.post("/comments_by_post_id", urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    Comments.getCommentsByPostId(req, res, req.body)
});

//Найти издателя
app.post("/publishers", urlencodedParser, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    User.getPublisherByName(req, res, req.body)
});

// Получить издателей
app.get('/publishers', urlencodedParser, (req,res) => {
    User.getPublishers(req, res);
});


//Изменить пароль пользователя
app.put("/users", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    User.updateUserPassword(req, res, req.body)
});

//Изменить статус пользователя
app.put("/usersstatus", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    User.updateUserStatus(req, res, req.body)
});

//Удалить пользователя
app.delete("/users", urlencodedParser, verifyToken, (req, res) => {
    if(!req.body) return res.sendStatus(400);
    User.deleteUser(req, res, req.body)
});

// Получить пользователей
app.get('/users', urlencodedParser, verifyToken, (req,res) => {
    User.getUsersWithModerate(req, res);
});

app.get('*', (req,res) => {
    res.send(res);
});

app.use(function(err, req, res, next) {
    res.json(err);
});

app.listen(8000, function(){
    console.log('Express server listening on port 8000');
});

function verifyToken (req, res, next) {
    let bearerHeader = req.headers['authorization']
    if(typeof bearerHeader !== 'undefined'){
        let bearer = bearerHeader.split(" ")
        req.token = bearer[1]
        User.checkToken(req, next)
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "FORBIDDEN"
            }
        );
    }
}

