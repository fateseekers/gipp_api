const jwt      = require('jsonwebtoken');

exports.generateToken = (id, userName, userRole, userStatus) => {
    let user = {
        id: id,
        name: userName,
        role: userRole,
        status: userStatus
    }
    return jwt.sign({
        // exp: Math.floor(Date.now() / 1000) + (60 * 60 * 48),
        user: user
    }, process.env.TOKEN_SECRET)
}

exports.decodeUserFromToken = (token) => {
    return jwt.verify(token, process.env.TOKEN_SECRET, function (err, decoded) {
        if (err) {
            return err
        } else {
            return decoded.user
        }
    })
}