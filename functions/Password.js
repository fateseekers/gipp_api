const bcrypt   = require('bcrypt');

const saltRounds = 10;

exports.hashPassword = (userPass) => {
    return bcrypt.hashSync(userPass, saltRounds)
}

exports.compareHashPasswords = (userPass, dbPass) => {
    return bcrypt.compareSync(userPass, dbPass);
}