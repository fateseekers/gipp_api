const db           = require('../../config/db');
const Token        = require('../../functions/Token');

exports.getGoods = (req, res) => {
    if(req.query.page !== undefined) {
        db.connection.query("SELECT COUNT(*) AS goods_count FROM `catalog`", function (err, result) {
            console.log(err)
            let count = result[0].goods_count
            db.connection.query("SELECT * FROM `catalog` ORDER BY `date` DESC LIMIT 10 OFFSET " + req.query.page, function (err, result) {
                console.log(err)
                if (result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK",
                            count: count,
                            goods: result
                        }
                    );
                }
            });
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.getGoodById = (req, res) => {
    if(req.query.id !== undefined) {
        db.connection.query('SELECT * FROM `catalog` WHERE `id`="' + req.query.id + '" AND `id` IS NOT NULL', function(err, result) {
            if(result) {
                res.statusCode = 200;
                res.send(
                    {
                        message: "OK",
                        good: result
                    }
                );
            }
        });
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.getGoodsByAuthorId = (req, res, data) => {
    db.connection.query(`SELECT COUNT(*) AS goods_count FROM catalog WHERE author_id=${data.author_id}`, function (err, result) {
        console.log(err)
        let count = result.goods_count,
            sql = 'SELECT * FROM `catalog` WHERE `author_id`="' + data.author_id + '" ORDER BY `date`' + (typeof (data.page) !== "undefined" ?  ('DESC LIMIT 10 OFFSET ' + data.page) : '')
        db.connection.query(sql, function (err, result) {
            console.log(err)
            if (result) {
                res.statusCode = 200;
                res.send(
                    {
                        message: "OK",
                        count: count,
                        goods: result
                    }
                );
            }
        });
    })
}

exports.addGood = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token),
        content = JSON.stringify(data.content)
    console.log(data)
    if(typeof (data.name && content && user.id && data.type && data.age && data.contacts && data.place) !== "undefined") {
        db.connection.query("INSERT INTO `catalog`(`name`, `content`, `contacts`, `type`, `age`, `place`, `publisher_name`, `author_id`) VALUES ('" + data.name + "', '" + content + "', '" + data.contacts + "', '" + data.type + "', '" + data.age + "', '" + data.place + "','" + (user.role === "Admin" ? 'Администратор' : user.name) + "', '" + user.id + "')", function (err, result) {
            console.log(err)
            if(result !== undefined) {
                if (result.affectedRows === 1) {
                    res.statusCode = 201;
                    res.send(
                        {
                            message: "ADDED"
                        }
                    );
                } else {
                    res.statusCode = 400;
                    res.send(
                        {
                            message: "BAD REQUEST"
                        }
                    )
                }
            }
        });
    } else {
        res.statusCode = 404;
        res.send(
            {
                message: "CONTENT NOT GIVEN"
            }
        )
    }
}


exports.deleteGood = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token)
    db.connection.query('SELECT * FROM `catalog` WHERE `id`="' + data.id + '"', function (err, result) {
        if (user.role === "Admin" || user.id === result[0].author_id) {
            db.connection.query('DELETE FROM `catalog` WHERE `id`="' + data.id + '" AND `id` IS NOT NULL', function (err, result) {
                if (result.affectedRows === 1) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "DELETED",
                        }
                    );
                } else {
                    res.statusCode = 404;
                    res.send(
                        {
                            message: "POST NOT FOUND"
                        }
                    );
                }
            });
        } else {
            res.statusCode = 101;
            res.send(
                {
                    message: "PERMISSIONS DENIED"
                }
            );
        }
    })
}

exports.updateGood = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token),
        content = JSON.stringify(data.content)

    if(typeof (data.name && content && user.id && data.type && data.age && data.contacts && data.place) !== "undefined") {
        db.connection.query("SELECT * FROM `catalog` WHERE `id`='" + data.id + "'", function (err, result) {
            if (user.role === "Admin" || user.id === result[0].author_id) {
                db.connection.query("UPDATE `catalog` SET `name`='" + data.name + "', `content`= '" + content + "', `contacts`='" + data.contacts + "', `type`='" + data.type + "', `age`='" + data.age + "', `place`='" + data.place + "' WHERE id='" + data.id + "'", function (err, result) {
                    console.log(err)

                    if (result) {
                        if (result.affectedRows === 1) {
                            res.statusCode = 200;
                            res.send(
                                {
                                    message: "UPDATED"
                                }
                            );
                        } else {
                            res.statusCode = 404;
                            res.send(
                                {
                                    message: "POST NOT FOUND"
                                }
                            );
                        }
                    }
                });
            } else {
                res.statusCode = 101;
                res.send(
                    {
                        message: "PERMISSIONS DENIED"
                    }
                );
            }
        })
    }  else {
        res.status(404);
        res.send(
            {
                message: "CONTENT NOT GIVEN"
            }
        )
    }
}

exports.getGoodByFilter = (req, res, data) => {
    if(data.age !== null && typeof data.age !== ("undefined" || "null")) {
        let countSql = "SELECT COUNT(*) AS `goods_count` FROM `catalog`"
        countSql += typeof data.age !== "undefined" ? (" WHERE `age`>=" + data.age) : "WHERE `age`>=0"
        countSql += typeof data.type !== "undefined" && data.type !== "" ? (" AND `type`='" + data.type + "'") : ""
        db.connection.query(countSql, function (err, result) {
            let count = result[0].goods_count,
                sql = "SELECT * FROM catalog"
            sql += typeof data.age !== "undefined" ? (" WHERE `age`>=" + data.age) : "WHERE `age`>=0"
            sql += typeof data.type !== "undefined" && data.type !== "" ? (" AND `type`='" + data.type + "'") : ""
            sql += " ORDER BY date DESC LIMIT 10 OFFSET " + data.page
            db.connection.query(sql, function (err, result) {
                console.log(err)
                if (result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK",
                            count: count,
                            goods: result
                        }
                    );
                }
            });
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}
