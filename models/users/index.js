const Token    = require('../../functions/Token')
const Password = require('../../functions/Password')
const db       = require('../../config/db');

exports.getUsersWithModerate = (req, res) => {
    let user = Token.decodeUserFromToken(req.token)
    if(user.role === "Admin") {
        db.connection.query('SELECT `id`, `name`, `email`, `description` FROM `users` WHERE `role`="Publisher" AND `status`="Moderate"', function (err, result) {
            res.statusCode = 200;
            res.send(
                {
                    message: "OK",
                    users: result
                }
            );
        });
    }
}

exports.getPublishers = (req, res) => {
    db.connection.query('SELECT `id`, `name`, `description` FROM `users` WHERE `role`="Publisher" AND `status`="Active"', function (err, result) {
        res.statusCode = 200;
        res.send(
            {
                message: "OK",
                publishers: result
            }
        );
    });
}

exports.getPublisherByName = (req, res, data) => {
    if(data.str) {
        db.connection.query('SELECT * FROM `users` WHERE `username` LIKE %"' + data.str + '"%', function (err, result) {
            res.statusCode = 200;
            res.send(
                {
                    message: "OK",
                    users: result
                }
            );
        });
    } else {
        res.statusCode = 402;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.regUser = (req, res, data) => {
    if(data.name && data.username && data.password && data.role) {
        db.connection.query('SELECT * FROM `users` WHERE `username`= "' + data.username + '"', function (err, result) {
	    if (result.length === 1) {
                res.statusCode = 403;
                res.send(
                    {
                        message: "USER CONSIST"
                    }
                );
            } else {
                let hashPassword = Password.hashPassword(data.password),
                    status = data.role === 'Publisher' ? 'Moderate' : 'Active'
                db.connection.query("INSERT INTO `users`(`name`, `username`, `email`, `description`, `password`, `role`, `status`) VALUES ('" + data.name + "','" + data.username + "','" + data.email + "','" + data.description + "','" + hashPassword + "','" + data.role + "','" + status + "')", function (err, result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK"
                        }
                    );
                })
            }
        });
    } else {
        res.statusCode = 400;
        res.send(
            {
                message: "DATA NOT GIVEN"
            }
        );
    }
}

exports.authUser = (req, res, data) => {
    if(data.username && data.password)
    db.connection.query('SELECT * FROM `users` WHERE `username`= "' + data.username + '"', function(err, result) {
        if(result.length === 1){
            if(!Password.compareHashPasswords(data.password, result[0].password)){
                res.statusCode = 403;
                res.send(
                    {
                        message: "PASSWORDS NOT EQUAL"
                    }
                );
            } else {
                let token = Token.generateToken(result[0].id, result[0].name, result[0].role, result[0].status);
                db.connection.query("UPDATE `users` SET `token`='" + token + "' WHERE `username`='" + result[0].username + "'", function(err, result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK",
                            token
                        }
                    );
                })
            }
        } else {
            res.statusCode = 404;
            res.send(
                {
                    message: "USER NOT FOUND"
                }
            );
        }
    });
}

exports.deleteUser = (req, res, data) => {
        let user = Token.decodeUserFromToken(req.token)
        if(user.role === "Admin") {
            db.connection.query('DELETE FROM `users` WHERE `id`="' + data.id + '"', function (err, result) {
                if (result.affectedRows === 1) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "DELETED"
                        }
                    );
                } else {
                    res.statusCode = 404;
                    res.send(
                        {
                            message: "USER NOT FOUND"
                        }
                    );
                }
            });
        } else {
            res.statusCode = 101;
            res.send(
                {
                    message: "PERMISSIONS DENIED"
                }
            );
        }
}

exports.updateUserPassword = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token)
    if(user.role === "Admin" || (user.id === data.id)) {
        db.connection.query("SELECT `password` FROM `users` WHERE `id`=" + data.id, function(err, result){
            if(Password.compareHashPasswords(data.oldpassword, result[0].password)){
                let hashNewPassword = Password.hashPassword(data.newpassword)

                db.connection.query('UPDATE `users` SET `password`="' + hashNewPassword + '" WHERE id="' + data.id + '" AND `id` IS NOT NULL', function (err, result) {
                    if (result.affectedRows === 1) {
                        res.statusCode = 200;
                        res.send(
                            {
                                message: "UPDATED"
                            }
                        );
                    } else {
                        res.statusCode = 404;
                        res.send(
                            {
                                message: "USER NOT FOUND"
                            }
                        );
                    }
                });
            } else {
                res.statusCode = 400;
                res.send(
                    {
                        message: "PASSWORDS DOESNT MATCH"
                    }
                );
            }
        })
    }
}

exports.updateUserStatus = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token)
    if(user.role === "Admin") {
        db.connection.query('UPDATE `users` SET `status`="' + data.status + '", `token`="NULL", `refresh`="NULL"  WHERE id="' + data.id + '" AND `id` IS NOT NULL', function (err, result) {
            if (result.affectedRows === 1) {
                res.statusCode = 200;
                res.send(
                    {
                        message: "UPDATED"
                    }
                );
            } else {
                res.statusCode = 404;
                res.send(
                    {
                        message: "USER NOT FOUND"
                    }
                );
            }
        });
    }
}

exports.checkToken = (req, next) => {
    let user = Token.decodeUserFromToken(req.token)

    db.connection.query('SELECT `token` FROM `users` WHERE `id`="' + user.id + '"', function (err, result) {
        if (result) {
            if(result[0].token === req.token) {
                next()
            }
        } else {
            res.statusCode = 403;
            res.send(
                {
                    message: "FORBIDDEN"
                }
            );
        }
    });
}
