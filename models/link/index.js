const needle  = require('needle');
const cheerio = require("cheerio");

exports.FetchLink = async (req, res) => {
    const urlToFetch = req.query.url
    let title = await needle('get', urlToFetch).then((res) => {
        let $ = cheerio.load(res.body),
            title = $('title').text()

        if(title.contains(/[0-9]/)){
            return ""
        }
        return title
    }).catch(function(err) {
        console.log(err)
    });

    res.send(
        {
            "success" : 1,
            "meta": {
                'title': title,
            }
        }
    )
}