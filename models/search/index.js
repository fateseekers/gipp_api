const db           = require('../../config/db');

exports.searchByAuthorAndPostTitle = (req, res, data) => {
    if(data.page !== undefined && typeof data.str !== "undefined") {
        db.connection.query(`SELECT COUNT(*) AS posts_count FROM posts WHERE title LIKE "%${data.str}%"`, function (err, result) {
            console.log(err)
            let count = result[0].posts_count
            db.connection.query(`SELECT * FROM posts WHERE title LIKE "%${data.str}%" ORDER BY date DESC LIMIT 10 OFFSET ${data.page}`, function (err, result) {
                console.log(result)
                let posts = result
                db.connection.query(`SELECT id, name, description FROM users WHERE name LIKE "%${data.str}%" AND role='Publisher' AND status='Active'`, function (err, result) {
                    console.log(result)
                    if (result) {
                        res.statusCode = 200;
                        res.send(
                            {
                                message: "OK",
                                count: count,
                                posts: posts,
                                publishers: result
                            }
                        );
                    }
                })
            });
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}
