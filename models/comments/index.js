const Token    = require('../../functions/Token')
const db       = require('../../config/db');

exports.getCommentsByPostId = (req, res, data) => {
    db.connection.query("SELECT comments.id, comments.text, comments.date, comments.author_id, users.name, users.role FROM `comments` AS comments LEFT JOIN `users` AS users ON users.id = comments.author_id WHERE comments.posts_id='" + data.id + "' GROUP BY comments.id, users.id ORDER BY `date` DESC", function(err, result) {
        if(result) {
            res.statusCode = 200;
            res.send(
                {
                    message: "OK",
                    comments: result
                }
            );
        }
    });
}

exports.addComment = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token)

    if(typeof (data.text && user.id && data.post_id) !== "undefined") {
        db.connection.query("INSERT INTO `comments`(`author_id`, `posts_id`, `text`) VALUES ('" + user.id + "', '" + data.post_id + "', '" + data.text + "')", function (err, result) {
            console.log(err)
            if(result !== undefined) {
                if (result.affectedRows === 1) {
                    res.statusCode = 201;
                    res.send(
                        {
                            message: "ADDED"
                        }
                    );
                } else {
                    res.statusCode = 400;
                    res.send(
                        {
                            message: "BAD REQUEST"
                        }
                    )
                }
            } else {
                res.statusCode = 401;
                res.send(
                    {
                        message: "BAD REQUEST"
                    }
                )
            }
        });
    } else {
        res.statusCode = 101;
        res.send(
            {
                message: "CONTENT NOT GIVEN"
            }
        )
    }
}


exports.deleteComment = (req, res, data) => {
    console.log(data)
    let user = Token.decodeUserFromToken(req.token)
    if(typeof (user.id && data.id) !== "undefined") {
        console.log("OK")
        db.connection.query('SELECT * FROM `comments` WHERE `id`="' + data.id + '"', function (err, result) {
            if (user.role === "Admin" || user.id === result[0].author_id) {
                db.connection.query('DELETE FROM `comments` WHERE `id`="' + data.id + '" AND `id` IS NOT NULL', function (err, result) {
                    if (result.affectedRows === 1) {
                        res.statusCode = 200;
                        res.send(
                            {
                                message: "DELETED",
                            }
                        );
                    } else {
                        res.statusCode = 404;
                        res.send(
                            {
                                message: "POST NOT FOUND"
                            }
                        );
                    }
                });
            } else {
                res.statusCode = 101;
                res.send(
                    {
                        message: "PERMISSIONS DENIED"
                    }
                );
            }
        })
    } else {
        res.statusCode = 101;
        res.send(
            {
                message: "PERMISSIONS DENIED"
            }
        );
    }
}

exports.updateComment = (req, res, data) => {
    let user = Token.decodeUserFromToken(req, res, req.token)
    if(data.id && data.posts_id && data.text && user.id) {
        db.connection.query("SELECT * FROM `comments` WHERE `id`='" + data.id + "'", function (err, result) {
            if (user.role === "admin" || user.id === result[0].author_id) {
                db.connection.query("UPDATE `comments` SET `text`='" + data.text + "' WHERE 1", function (err, result) {
                    if (result !== undefined) {
                        if (result.affectedRows === 1) {
                            res.statusCode = 200;
                            res.send(
                                {
                                    message: "UPDATED"
                                }
                            );
                        } else {
                            res.statusCode = 404;
                            res.send(
                                {
                                    message: "POST NOT FOUND"
                                }
                            );
                        }
                    }
                });
            } else {
                res.statusCode = 101;
                res.send(
                    {
                        message: "PERMISSIONS DENIED"
                    }
                );
            }
        })
    }  else {
        res.statusCode = 404;
        res.send(
            {
                message: "CONTENT NOT GIVEN"
            }
        )
    }
}