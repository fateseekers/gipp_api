const Token    = require('../../functions/Token')
const db       = require('../../config/db');

exports.giveLike = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token)
    if(typeof (user && data.id) !== "undefined"){
        console.log(user.id)
        db.connection.query("SELECT * FROM `likes` WHERE `user_id`='" + user.id + "' AND `posts_id`='" + data.id + "'", function(err, result) {
            if(result.length === 0) {
                db.connection.query("INSERT INTO `likes`(`user_id`, `posts_id`, `rating`) VALUES ('" + user.id + "', '" + data.id + "', '1')", function (err, result) {
                    if (result) {
                        res.status(200);
                        res.send(
                            {
                                message: "LIKED"
                            }
                        );
                    }
                });
            } else {
                db.connection.query(`UPDATE likes SET rating=${result[0].rating === 1 ? 0 : 1 } WHERE posts_id=${data.id} AND user_id=${user.id}`, function (err, result) {
                    if (result) {
                        db.connection.query("SELECT * FROM `likes` WHERE `user_id`='" + user.id + "' AND `posts_id`='" + data.id + "'", function(err, result){
                            res.status(200);
                            res.send(
                                {
                                    message: result[0].rating === 1 ? "LIKED" : "DISLIKED"
                                }
                            );
                        })
                    }
                });
            }
        })
    } else {
        res.status(400);
        res.send(
            {
                message: "PERMISSIONS DENIED"
            }
        );
    }
}

exports.getLikesFromPost = (req, res, data) => {
    if(data.id !== undefined) {
        db.connection.query("SELECT COUNT(*) AS likes_count FROM `likes` WHERE `posts_id`='" + data.id + "' AND `rating`=1", function (err, result) {
            console.log(result)
            if (result) {
                res.statusCode = 200;
                res.send(
                    {
                        message: "OK",
                        likes: result[0].likes_count
                    }
                );
            }
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}