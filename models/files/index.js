const Token    = require('../../functions/Token')
const path     = require('path');
const average = require('image-average-color');

exports.uploadByFile = (req, res) => {
    let user = Token.decodeUserFromToken(req.token)
    if(user.role === "Admin" || "Publisher") {
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send('No files were uploaded.');
        }
        let sampleFile = req.files.image,
            time = Date.now()
        sampleFile.mv(path.join('tmp/' + sampleFile.name + time), function (err) {
            if (err) {
                return res.status(500).send(err);
            }
            let colorArr =  average(sampleFile.data, (err, color) => {
                if (err) throw err;
                let [red, green, blue, alpha] = color;
                res.send({
                    success: 1,
                    file: {
                        url: 'http://' + req.headers.host + '/api/tmp/' + sampleFile.name + time,
                        rgb: `${color[0]}, ${color[1]}, ${color[2]}`
                    }
                });
            });
        });
    }
}

