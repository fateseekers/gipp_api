const db           = require('../../config/db');
const Token        = require('../../functions/Token');

exports.getAllPosts = (req, res) => {
    console.log(req.query.page)
    if(req.query.page !== undefined) {
        db.connection.query("SELECT COUNT(*) AS posts_count FROM `posts`", function (err, result) {
            console.log(err)
            console.log(result)
            let count = result[0].posts_count
            db.connection.query("SELECT * FROM `posts` ORDER BY `date` DESC LIMIT 10 OFFSET " + req.query.page, function (err, result) {
                console.log(result)
                console.log(err)
                if (result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK",
                            count: count,
                            posts: result
                        }
                    );
                }
            });
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.getPosts = (req, res) => {
    console.log(req.query.page)
    if(req.query.page !== undefined) {
        db.connection.query("SELECT COUNT(*) AS posts_count FROM `posts` WHERE `status`<>'main'", function (err, result) {
        console.log(err)
	    console.log(result)
	    let count = result[0].posts_count
            db.connection.query("SELECT * FROM `posts` WHERE `status`<>'main' ORDER BY `date` DESC LIMIT 10 OFFSET " + req.query.page, function (err, result) {
                console.log(result)
                console.log(err)
                if (result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK",
                            count: count,
                            posts: result
                        }
                    );
                }
            });
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.getMainPosts = (req, res) => {
    if(req.query.page !== undefined) {
        db.connection.query("SELECT COUNT(*) AS posts_count FROM `posts` WHERE `status`='main'", function (err, result) {
            let count = result.posts_count
            db.connection.query("SELECT * FROM `posts` WHERE `status`='main' ORDER BY `date` DESC LIMIT 10 OFFSET " + req.query.page, function (err, result) {
                if (result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK",
                            count: count,
                            posts: result
                        }
                    );
                }
            });
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.getPostById = (req, res) => {
    if(req.query.id !== undefined) {
        db.connection.query('SELECT * FROM `posts` WHERE `id`="' + req.query.id + '" AND `id` IS NOT NULL', function (err, result) {
            if (result) {
                res.statusCode = 200;
                res.send(
                    {
                        message: "OK",
                        post: result
                    }
                );
            }
        });
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.getPostsByAuthorId = (req, res, data) => {
    if(typeof data.author_id !== "undefined") {
        db.connection.query(`SELECT COUNT(*) AS posts_count FROM posts WHERE author_id=${data.author_id}`, function (err, result) {
            console.log(err)
            let count = result.posts_count,
                sql = 'SELECT * FROM `posts` WHERE `author_id`="' + data.author_id + '" ORDER BY `date`' + (typeof (data.page) !== "undefined" ? ('DESC LIMIT 10 OFFSET ' + data.page) : '')
            db.connection.query(sql, function (err, result) {
                if (result) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "OK",
                            count: count,
                            posts: result
                        }
                    );
                }
            });
        })
    } else {
        res.statusCode = 403;
        res.send(
            {
                message: "BAD REQUEST"
            }
        );
    }
}

exports.addPost = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token),
        content = JSON.stringify(data.content),
        title = data.title

    if(typeof (title && content && user.id && data.status && data.age && data.signature) !== "undefined") {
        db.connection.query("INSERT INTO `posts`(`title`, `content`, `signature`, `age`, `status`, `author_id`) VALUES ('" + title + "', '" + content + "', '" + data.signature + "','" + data.age + "', '" + data.status + "', '" + user.id + "')", function (err, result) {
            console.log(err)
            if(result !== undefined) {
                if (result.affectedRows === 1) {
                    res.statusCode = 201;
                    res.send(
                        {
                            message: "ADDED"
                        }
                    );
                } else {
                    res.statusCode = 400;
                    res.send(
                        {
                            message: "BAD REQUEST"
                        }
                    )
                }
            }
        });
    } else {
        res.statusCode = 404;
        res.send(
            {
                message: "CONTENT NOT GIVEN"
            }
        )
    }
}


exports.deletePost = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token)
    db.connection.query('SELECT * FROM `posts` WHERE `id`="' + data.id + '"', function (err, result) {
        if (user.role === "Admin" || user.id === result[0].author_id) {
            db.connection.query('DELETE FROM `posts` WHERE `id`="' + data.id + '" AND `id` IS NOT NULL', function (err, result) {
                if (result.affectedRows === 1) {
                    res.statusCode = 200;
                    res.send(
                        {
                            message: "DELETED",
                        }
                    );
                } else {
                    res.statusCode = 404;
                    res.send(
                        {
                            message: "POST NOT FOUND"
                        }
                    );
                }
            });
        } else {
            res.statusCode = 101;
            res.send(
                {
                    message: "PERMISSIONS DENIED"
                }
            );
        }
    })
}

exports.updatePost = (req, res, data) => {
    let user = Token.decodeUserFromToken(req.token),
        content = JSON.stringify(data.content)

    if(typeof (data.id && content && user.id && data.status && data.age && data.signature) !== "undefined") {
        console.log(content)

        db.connection.query("SELECT * FROM `posts` WHERE `id`='" + data.id + "'", function (err, result) {
            if (user.role === "Admin" || user.id === result[0].author_id) {
                db.connection.query("UPDATE `posts` SET `title`='" + data.title + "',`content`='" + content + "', `signature`='" + data.signature + "', `author_id`='" + user.id + "', `status`='" + data.status + "', `age`='" + data.age + "' WHERE `id`='" + data.id + "'", function (err, result) {
                    console.log(err)
                    if (result !== undefined) {
                        if (result.affectedRows === 1) {
                            res.statusCode = 200;
                            res.send(
                                {
                                    message: "UPDATED"
                                }
                            );
                        } else {
                            res.statusCode = 404;
                            res.send(
                                {
                                    message: "POST NOT FOUND"
                                }
                            );
                        }
                    }
                });
            } else {
                res.statusCode = 101;
                res.send(
                    {
                        message: "PERMISSIONS DENIED"
                    }
                );
            }
        })
    }  else {
        res.status(404);
        res.send(
            {
                message: "CONTENT NOT GIVEN"
            }
        )
    }
}
