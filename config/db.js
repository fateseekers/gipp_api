const mysql           = require('mysql');

const connection = mysql.createPool({
    host     : process.env.DB_HOST,
    user     : process.env.DB_USER,
    password : process.env.DB_PASS,
    database : process.env.DB_DATABASE,
});

connection.getConnection(function (err, result){
    if(err){
        console.log(err)
        return
    }
})

exports.connection = connection;
